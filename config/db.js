const mysql = require('mysql');

const db = mysql.createConnection({
    host     : 'localhost',
    // port    : '3306',
    user     : 'root',
    password : '1234',
    database: 'movies'
  })

db.connect((err) =>{
    if(err){ 
        console.error('error connecting: ' + err.stack)
        return
    }
    // console.log('connected as id ' + db.threadId)
})

// db.end() 
module.exports = db