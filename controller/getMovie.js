const db = require('../config/db');

async function getMovie(req,res) {
    try{

        const sql = "SELECT * FROM movie_records ORDER BY mr_year DESC"
        db.query(sql, (error, results, fields) => {
            if (error) {
                return db.rollback(function () {
                    throw error
                })
            }

            res.status(200).send({
                success:true,
                movies:results.map((result) => {
                    // console.log(result)
                    return {
                        id:result.mr_id,
                        title:result.mr_title,
                        year:result.mr_year,
                        rating:result.mr_rating
                    }
                })
            })
        })
     

    } catch (e) {
        console.log(e)
    }
}

module.exports = {
    getMovie
}