const db = require('../config/db');
const bcrypt = require('bcrypt')

async function login(req, res) {
    try {

        var auth = Buffer.from((req.headers.authorization.split(' ')[1]), 'base64').toString('ascii')
        // console.log(Buffer.from(auth).toString('base64'))

        var username = auth.split(':')[0]
        var password = auth.split(':')[1]
        const sql = "SELECT * FROM users WHERE username ='" + username + "'"
        db.query(sql, (error, results, fields) => {
            if (error) {
                return db.rollback(function () {
                    throw error
                })
            }

            if (results.length > 0) {

                if (bcrypt.compareSync(password, results[0].password)) {
                    return res.status(200).send({
                        success: true,
                        id: results[0].user_id,
                        username: results[0].username,
                        role:results[0].role
                    })
                } else {
                    return res.status(200).send({
                        success: false,
                        message: "Wrong password"
                    })
                }
            } else {
                return res.status(200).send({
                    success: false,
                    message: "Username doesn't has in database"
                })
            }
        })
    } catch (e) {
        console.log(e)
        res.status(400).send({
            error: e.message
        })
    }

}

module.exports = {
    login
}