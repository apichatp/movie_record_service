const db = require('../config/db');
const bcrypt = require('bcrypt');

async function register(req, res) {
    try {
        var auth = Buffer.from((req.headers.authorization.split(' ')[1]), 'base64').toString('ascii').split(':')
        var role = req.body.role

        const check = "SELECT * FROM users WHERE username = '" + auth[0] + "'"
        db.query(check, (error, results, fields) => {
            if (error) {
                return db.rollback(function () {
                    throw error
                })
            }

            if (results.length === 0) {
                const sql = "INSERT INTO users (username,password , role) VALUES ('" + auth[0] + "', '" + bcrypt.hashSync(auth[1], 10) + "', '" + role + "')"
                db.query(sql, (error, results, fields) => {
                    if (error) {
                        return db.rollback(function () {
                            throw error
                        })
                    }

                    if (results) {
                        return res.status(200).send({
                            success: true,
                            // result:results,
                            id: results.insertId,
                            username: auth[0],
                            role:role
                        });
                    }
                })
            } else {
                return res.status(400).send({
                    success: false,
                    message: 'this username already in database.'
                })
            }
        })


    } catch (e) {
        console.log(e)
    }
}

module.exports = {
    register
}