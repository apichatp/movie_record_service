const db = require('../config/db');

async function deleteMovie(req,res) {
    try{
        if(req.body.id && req.body.id > 0){
            const sql = "DELETE FROM movie_records WHERE mr_id ="+req.body.id
            db.query(sql, (error, results, fields) => {
                if (error) {
                    return db.rollback(function () {
                        throw error
                    })
                }
    
                if(results.affectedRows > 0){
                    return res.status(200).send({
                        success:true,
                        message:'delete success'
                        
                    })
                }else {
                    return res.status(200).send({
                        succes:true,
                        message: 'No movie in database'
                    })
                }
                
            })
        }else{
            return res.status(400).send({
                succes:false,
                message:'please insert movie_id'
            })
        }
        
     

    } catch (e) {
        console.log(e)
    }
}

module.exports = {
    deleteMovie
}