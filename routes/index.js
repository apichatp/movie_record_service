var express = require('express');
var router = express.Router();
var {login} = require('../controller/login');
var {register} = require('../controller/register');
var {getMovie} = require('../controller/getMovie');
var {addMovie} = require('../controller/addMovie');
var {deleteMovie} = require('../controller/deleteMovie');
var {updateMovie} = require('../controller/updateMovie');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/login', login);
router.post('/register',register);
router.get('/getMovie',getMovie)
router.post('/addMovie',addMovie);
router.post('/deleteMovie',deleteMovie);
router.post('/updateMovie',updateMovie);


module.exports = router;
